import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
from math import ceil
from matplotlib import gridspec
from matplotlib.ticker import FormatStrFormatter

def get_study_dirs(study_name):
     """Returns a list of directories from the current working directory 
        with study_name in directory name."""

     return [dir_name for dir_name in os.listdir(os.curdir) 
             if study_name in dir_name and 
             os.path.isdir(dir_name)]

def setfield_call_list(surface, surface_params):
    """Returns a call list for aiFoamSetField for a surface and its parameters."""
    return ["aiFoamSetField", "-surfaceType", "%s" % surface, 
            "-surfaceParams", "%s" % surface_params, 
            "-volFieldName", "psi_c"]

def get_study_dframe(study_pattern, data_file_name, index_cols):

    data_dirs = [item for item in os.listdir(os.curdir) 
                 if os.path.isfile(os.path.join(item, data_file_name))
                 and study_pattern in item]
    
    data_dirs.sort()
    
    study_dframe = pd.DataFrame()
    
    for item in data_dirs:
        case_dframe = pd.read_csv(os.path.join(item, data_file_name), index_col=index_cols)
        study_dframe = pd.concat([study_dframe, case_dframe])
        
    # Dump data for backup
    study_dframe.to_csv("convergence2D_00.csv")
    study_dframe.to_latex("convergence2D_00.tex")
    
    return study_dframe

def first_order_convergent(h_old, h_new, e_old):
    return np.exp(np.log(h_new/h_old))*e_old

def second_order_convergent(h_old, h_new, e_old):
    return np.exp(2*np.log(h_new/h_old))*e_old

def plot_study(study_pattern, data_file_name, index_cols):

    study_metadata = pd.read_csv(study_pattern.rstrip("_") + "_parameters.csv")
    print ("Parameter study metadata:")
    print(study_metadata)
    
    study_dframe = get_study_dframe(study_pattern, data_file_name, index_cols)
    study_dframe.to_csv(study_pattern + "_dframe.csv")
    
    print(study_dframe)
    
    study_dframe = study_dframe.sort_index()
    unique_index = study_dframe.index.unique()
    n_subplots = len(unique_index)
    n_cols = 3
    n_rows = ceil(n_subplots / n_cols)
    legend_handles = []
    legend_labels = []
    fig = plt.figure()
    for idx,index_tuple in enumerate(unique_index): 
        ax = fig.add_subplot(n_rows, n_cols, idx + 1)
        ax.loglog()
        plot_data = study_dframe.loc[index_tuple]
        h_old = plot_data["DELTA_X"].iloc[0]
        h_new = plot_data["DELTA_X"].iloc[-1]
        y_min = plot_data.groupby(['DELTA_X']).min()['L_INF_ERROR']
        y_max = plot_data.groupby(['DELTA_X']).max()['L_INF_ERROR']
        y_mean = (y_max + y_min)/2
        y_err_plus = y_max - y_mean
        y_err_minus = y_mean - y_min
        ax.errorbar(y_mean.index, y_mean, yerr=[y_err_minus, y_err_plus], marker='o', label='L_INF_ERROR', capsize=8)
        # ax.plot(plot_data["DELTA_X"], plot_data["L_INF_ERROR"],'bo', label='L_INF_ERROR')
        
        y_min = plot_data.groupby(['DELTA_X']).min()['L_INF_REL_ERROR']
        y_max = plot_data.groupby(['DELTA_X']).max()['L_INF_REL_ERROR']
        y_mean = (y_max + y_min)/2
        y_err_plus = y_max - y_mean
        y_err_minus = y_mean - y_min
        ax.errorbar(y_mean.index, y_mean, yerr=[y_err_minus, y_err_plus], marker='o', label='L_INF_REL_ERROR', capsize=8)
        # ax.plot(plot_data["DELTA_X"], plot_data["L_INF_REL_ERROR"],'go', label='L_INF_REL_ERROR')
        
        y_min = plot_data.groupby(['DELTA_X']).min()['MEAN_REL_ERROR']
        y_max = plot_data.groupby(['DELTA_X']).max()['MEAN_REL_ERROR']
        y_mean = (y_max + y_min)/2
        y_err_plus = y_max - y_mean
        y_err_minus = y_mean - y_min
        ax.errorbar(y_mean.index, y_mean, yerr=[y_err_minus, y_err_plus], marker='o', label='MEAN_REL_ERROR', capsize=8)
        # ax.plot(plot_data["DELTA_X"], plot_data["MEAN_REL_ERROR"],'ro', label='MEAN_REL_ERROR')
        ax.plot([h_old, h_new],
                [plot_data["MEAN_REL_ERROR"].iloc[0], 
                second_order_convergent(h_old, h_new, plot_data["MEAN_REL_ERROR"].iloc[-1])], 
                'k:', label='MEAN_REL_ERROR second-order')
        ax.plot([h_old, h_new],
                [plot_data["MEAN_REL_ERROR"].iloc[0], 
                first_order_convergent(h_old, h_new, plot_data["MEAN_REL_ERROR"].iloc[-1])], 
                'k--', label='MEAN_REL_ERROR first-order')
        ax.set_title(" ".join(index_tuple)) 
        ax.xaxis.grid(True, which='major')
        if (idx == 0):
            legend_handles, legend_labels = ax.get_legend_handles_labels()
        xticks = plot_data["DELTA_X"].to_numpy()
        xtick_labels = ['$%d^{-1}$' % tick**-1 for tick in xticks]
        ax.set_xticks(xticks)
        ax.set_xticklabels(xtick_labels)

    fig.tight_layout()
    fig.legend(legend_handles, legend_labels,  
               loc='lower center', bbox_to_anchor=(0.35,-0.15),
               fancybox=True, shadow=True, ncol=2)
    fig.savefig(study_pattern + ".pdf", bbox_inches="tight")
    fig.savefig(study_pattern + ".png", bbox_inches="tight")