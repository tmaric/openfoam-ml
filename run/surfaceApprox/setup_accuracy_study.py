#!/usr/bin/python 

from optparse import OptionParser
import sys
import os
from subprocess import call
import csv 
from ai_foam import get_study_dirs, setfield_call_list

usage = """Generates a 2D or 3D parameter variation for an implicit
surface (plane, sphere, ellipsoid, sinc)."""

parser = OptionParser(usage=usage)

parser.add_option("-d", "--dimension", dest="dimension",
                  help="Dimension: '2D' or '3D'.", 
                  metavar="DIMENSION")

parser.add_option("-s", "--surface", dest="surface",
                  help="Surface: plane, sphere, ellipsoid, sinc.", 
                  metavar="SURFACE")

def main(args):
    (options, args) = parser.parse_args()

    if ((options.dimension is None) or (options.surface is None)):
        print("Dimension or surface not provided. Use --help for more information.")
        sys.exit(1)

    study_name = "accuracy_%s_%s" % (options.dimension, options.surface)
    param_filename = "accuracy_%s.parameter" % options.dimension

    # Create OpenFOAM parameter study folders
    call(["../create_study.py", "-s", study_name, "-p", param_filename, 
          "-c", "unit_box_domain", "-d", options.dimension])

    # Create meshes 
    call(["../bulkeval", study_name, "blockMesh"])

    # Initialize the implicit surface field values
    surface_params = ""
    ## Set surface parameters for the plane
    if (options.surface == "plane"):

        plane_center = ""
        plane_normal = ""

        if (options.dimension == "2D"):
            plane_center = "(0.5 0.5 0)"
            plane_normal = "(1 2 0)"

        elif(options.dimension == "3D"):
            plane_center = "(0.5 0.5 0.5)"
            plane_normal = "(1 2 3)"

        surface_params = plane_center + " " + plane_normal

    ## Set surface parameters for the sphere
    elif (options.surface == "sphere"):

        if (options.dimension == "2D"):
            surface_params = "(0.5 0.5 0) 0.25"
        elif (options.dimension == "3D"):
            surface_params = "(0.5 0.5 0.5) 0.25"

    ## Set surface parameters for the ellipsoid
    elif (options.surface == "ellipsoid"): 

        ellipsoid_center = ""
        if (options.dimension == "2D"):
            ellipsoid_center = "(0.5 0.5 0)" 
        elif (options.dimension == "3D"):
            ellipsoid_center = "(0.5 0.5 0.5)" 

        ellipsoid_axes = "(0.4 0.3 0.2)"
        surface_params = ellipsoid_center + " " + ellipsoid_axes 

    ## Set surface parameters for the 3D sinc function
    elif (options.surface == "sinc"):
        
        if (options.dimension == "2D"):
            print("sinc function currently supported only in 2D.")
            sys.exit(1)
        if (options.dimension == "3D"):
            surface_params = "(0.5 0.5 0.5) 0.25 25" 
    else: 
        print("Available surfaces are: plane, sphere, ellipsoid and sinc.")
        sys.exit(1)
        

    ## Run field initalization and training 
    study_dirs = get_study_dirs(study_name)
    parent_dir = os.getcwd()
    for study_dir in study_dirs: 
        os.chdir(study_dir)
        call(setfield_call_list(options.surface, surface_params))
        os.chdir(parent_dir)
    

if __name__ == "__main__":
    main(sys.argv[1:])

