#!/bin/bash
dim=2D
if [ "$2" = "3D" ]; then
	dim=3D
fi
echo $dim
if [ "$1" = "sinc" ]; then
        ../create-study.py -s convergence${dim}_sinc -p unit_box_domain.parameter -c unit_box_domain -d $dim
        ../bulkeval convergence${dim}_sinc_0000 "blockMesh"
        ../bulkeval convergence${dim}_sinc_0000 "aiFoamSetField -surfaceType $1 -surfaceParams '(0.5 0.5 0) 0.5 50' -volFieldName psi_c"
elif [ "$1" = "plane" ]; then
        ../create-study.py -s convergence${dim}_plane -p unit_box_domain.parameter -c unit_box_domain -d $dim
        ../bulkeval convergence${dim}_plane_0000 "blockMesh"
        ../bulkeval convergence${dim}_plane_0000 "aiFoamSetField -surfaceType $1 -surfaceParams '(0.5 0.5 0) (0 1 0)' -volFieldName psi_c"
elif [ "$1" = "sphere" ]; then
        ../create-study.py -s convergence${dim}_sphere -p unit_box_domain.parameter -c unit_box_domain -d "$dim"
        ../bulkeval convergence${dim}_sphere_0000 "blockMesh"
        ../bulkeval convergence${dim}_sphere_0000 "aiFoamSetField -surfaceType $1 -surfaceParams '(0.5 0.5 0) 0.25' -volFieldName psi_c"
elif [ "$1" = "ellipsoid" ]; then
	../create-study.py -s convergence${dim}_ellipsoid -p unit_box_domain.parameter -c unit_box_domain -d "$dim"
        ../bulkeval convergence${dim}_ellipsoid_0000 "blockMesh"
	../bulkeval convergence${dim}_ellipsoid_0000 "aiFoamSetField -surfaceType $1 -surfaceParams '(0.5 0.5 0) (0.25 0 0) (0.1 0 0)' -volFieldName psi_c"
else
        echo "Error: name of the surface not provided as first argument." 
        echo "Available surfaces: sinc, plane, sphere."
        exit 1
fi

