#!/usr/bin/python 

from optparse import OptionParser
import sys
import os
from subprocess import call
import csv 
from ai_foam import get_study_dirs 

usage = """Traings a neural network on an implicit surface  
(plane, sphere, ellipsoid, sinc) whose values are stored at 
finite volume centers."""

parser = OptionParser(usage=usage)

parser.add_option("-s", "--study-pattern", dest="study_pattern",
                  help="Pattern that matches to parameter study folders.", 
                  metavar="STUDY_PATTERN")

parser.add_option("-a", "--approximator", dest="approximator",
                  help="Neural network finite volume field approximator. Currently only ISO-MLP is available.", 
                  metavar="APPROXIMATOR")

parser.add_option("-l", "--hidden-layers", dest="hidden_layers",
                  help="Neural network architecture. Number of layers has to be placed inside \'( )\'", 
                  metavar="HIDDEN_LAYERS")

parser.add_option("-e", "--epsilon", dest="epsilon",
                  default=1e-05,
                  help="Neural network solver tolerance.", 
                  metavar="EPSILON")

parser.add_option("-i", "--max-iterations", dest="max_iterations",
                  default=2000,
                  help="Neural network solver max iterations.",                     
                  metavar="MAX_ITERATIONS")

parser.add_option("-o", "--optimizer-step", dest="optimizer_step",
                  default=0.01,
                  help="Neural network solver step.", 
                  metavar="OPTIMIZER_STEP")

def main(args):
    (options, args) = parser.parse_args()

    if ((options.approximator is None) or 
        (options.hidden_layers is None) or
        (options.study_pattern is None)):

        print("Required options not used: study-pattern, approximator, or hidden-layers.")
        print("Use --help option for more information.")
        sys.exit(1)

    ## Append parameter study metadata into the metadata CSV file
    metadata_filename = options.study_pattern + "_parameters.csv"

    csv_writer = []
    if (not os.path.isfile(metadata_filename)):
        with open(metadata_filename, 'w', newline='') as csvfile:
            csv_writer = csv.writer(csvfile, delimiter=',')
            csv_writer.writerow(['APPROXIMATOR', 'HIDDEN_LAYERS', 
                                 'EPSILON', 'OPTIMIZER_STEP', 
                                 'MAX_ITERATIONS'])

    with open(metadata_filename, 'a', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',')
        csv_writer.writerow([options.approximator, options.hidden_layers,
                             options.epsilon, options.optimizer_step, 
                             options.max_iterations]) 

    ## Run field initalization and training 
    study_dirs = get_study_dirs(options.study_pattern)
    parent_dir = os.getcwd()
    for study_dir in study_dirs: 
        os.chdir(study_dir)
        call(["aiFoamLearn", 
              "-approximator", "%s" % options.approximator,
              "-hiddenLayers", "%s" % options.hidden_layers, 
              "-epsilon", "%s" % options.epsilon,
              "-optimizerStep", "%s" % options.optimizer_step,
              "-maxIterations", "%s" % options.max_iterations,
              "-volFieldName", "psi_c"])
        os.chdir(parent_dir)
    

if __name__ == "__main__":
    main(sys.argv[1:])

