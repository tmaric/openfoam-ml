template<typename Function>
torch::nn::Sequential ai_foam_learn(
    Function func,
    double xa, 
    double xb, 
    long int nx
)
{
    // Constructing the model using `torch::Sequential` 
    torch::nn::Sequential model (
        torch::nn::Linear(1,64), 
        torch::nn::Tanh(),
        torch::nn::Linear(64,1)
    );

    // Preparing sample data 
    torch::Tensor x_sequence = torch::linspace(xa, xb, nx);

    // Reshape and save x_sequence 
    x_sequence = x_sequence.reshape({nx,1});
    torch::save(x_sequence, "x_" + func.name() + "_sequence.pt");

    // Reshape and save y_sequence 
    torch::Tensor y_sequence = func(x_sequence); 
    torch::save(y_sequence, "y_" + func.name() + "_sequence.pt");

    // SAMPLE DATA: x, func(x)
    // Training set 70 / 30 split 
    torch::Tensor shuffled_indices = torch::randperm(
        nx, 
        torch::TensorOptions().dtype(at::kLong)
    ); 

    auto n_val = int (0.7 * nx);
    torch::Tensor training_indices = 
        shuffled_indices.index({Slice(0, n_val)}); 
    torch::Tensor x_training = x_sequence.index(training_indices);
    torch::Tensor y_training = y_sequence.index(training_indices);

    // TRAIN THE MODEL ON THE TRAINING SET
    torch::optim::Adam optimizer(model->parameters(), 0.01);
    torch::Tensor training_prediction = torch::zeros_like(x_training);
    torch::Tensor loss_values = torch::zeros_like(x_training);

    ofstream conv_file (func.name() + "_convergence_data.csv"); 
    conv_file << "max_loss\n";
    for (size_t epoch = 1; epoch <= 5000; ++epoch) 
    {
        optimizer.zero_grad();

        training_prediction = model->forward(x_training);

        loss_values = torch::mse_loss(training_prediction, y_training); 
        loss_values.backward(); 

        optimizer.step();

        // Report the error with respect to y_training. 
        double mse_loss = loss_values.item<double>();
        cout << "Epoch " << epoch 
            << ", mse loss = " << mse_loss << endl;
        conv_file << mse_loss << "\n";
    }

    // VALIDATE THE MODEL WITH THE VALIDATION SET
    torch::Tensor validation_indices = 
        shuffled_indices.index({Slice(n_val+1, nx)}); 
    torch::Tensor x_validation = 
        x_sequence.index(validation_indices);
    torch::Tensor y_validation = 
        y_sequence.index(validation_indices);
    torch::Tensor validation_values = 
        model->forward(x_validation); 
    torch::Tensor validation_loss = 
        torch::mse_loss(validation_values, y_validation); 

    cout << "Validation max(validation_loss) = " 
        << torch::max(validation_loss) << endl;

    // REPORT THE PREDICTION OVER COMPLETE INPUT
    torch::Tensor y_model_sequence = model->forward(x_sequence); 
    torch::save(y_model_sequence, "y_" + func.name() + "_model_sequence.pt");
    return model;
}

void ai_foam_test_scalar_autograd()
{
    // sin(x), sin'(x), sin''(x)
    {
        auto x = torch::zeros({1}, torch::requires_grad());
        auto sinx = torch::sin(x); 

        // Alternative. 
        //sinx.backward(); 
        //auto dsinx = x.grad()[0];
        //dsinx.set_requires_grad(true);
        
        auto dsinx = torch::autograd::grad({sinx}, {x}, {}, true, true)[0]; 
        auto dsinx_e = torch::cos(x);
        
        auto dsinx_error = torch::abs(dsinx_e - dsinx).item<double>();
        std::cout << std::setprecision(20) 
            << "dsinx_error = " << dsinx_error << "\n" ;
        assert(dsinx_error == 0);

        // Alternative.
        //dsinx.backward(); 
        //auto ddsinx = dsinx.grad(); 
        //ddsinx.requires_grad(); 
        
        auto ddsinx = torch::autograd::grad({dsinx}, {x})[0]; 
        auto ddsinx_e =  -torch::sin(x); 

        auto ddsinx_error = torch::abs(ddsinx_e - ddsinx).item<double>();
        std::cout 
            << "ddsinx_error = " << ddsinx_error << "\n"; 
        assert(ddsinx_error == 0);
    }

    // https://youtu.be/R_m4kanPy6Q?t=458
    // f(x,y) = (x + y) * (y + 3)
    // \partial_x f(x,y) = y + 3 
    // \partial_y f(x,y) = 2y + x + 3 
    // for x = 1, y = 2, 
    // \partial_x f(x,y) = 2 + 3 = 5
    // \partial_y f(x,y) = 2*2 + 1 + 3 = 8
    {
        auto x = torch::ones(1, torch::requires_grad());
        auto y = torch::full_like(x, 2., torch::requires_grad());
        auto f = (x + y) * (y + 3);

        auto partial_x_f = torch::autograd::grad({f}, {x}, {}, true, true)[0];
        assert((partial_x_f.item<double>() == 5));

        auto partial_y_f = torch::autograd::grad({f}, {y})[0];
        assert((partial_y_f.item<double>() == 8));
    }
}

void ai_foam_test_vector_autograd()
{
    // x in R^3, f = dot(x,x) 
    // partial_x f = [2e 2e 2e]
    // if e1 = (1,1,1)
    // partial_x f = [2 2 2]
    {
        //std::vector<double> e {1, 1, 1}; 
        //auto x = torch::from_blob(e.data(),3,torch::requires_grad());
        auto x = torch::ones(3,torch::requires_grad());
        auto f = dot(x,x); 
        auto grad_f_x = torch::autograd::grad({f}, {x}, {}, true, true);
        std::cout << "grad_f_x = \n" << grad_f_x << std::endl; 

        
        auto div_grad_f_x_v =  torch::autograd::grad(
            {grad_f_x}, {x}, {torch::ones(3)}, true, true
        );
        std::cout << "div(grad(f)) = (J_{grad_f} . [1 1 1]) . [1 1 1] = " 
            << div_grad_f_x_v[0].sum() << endl;

        auto diag_grad2_f_x = torch::autograd::grad(
            {grad_f_x}, {x}, {torch::eye(3)}
        );
        auto div_grad_f_x = diag_grad2_f_x[0].sum();
        std::cout << "div(grad(f)) = (J_{grad_f} . I) . [1 1 1] = " 
            << div_grad_f_x << std::endl;
    }

    // Calculate y = sin(x), x,y in R^n , such that y_i = sin(x_i)
    // Then calculate y'_i = d sin(x_i) / dx_i
    {
        // Input vector x 
        auto x = torch::linspace(0, M_PI, 100, torch::requires_grad()); 

        // sin(x) as a vector-valued function 
        auto sinx = torch::sin(x); 

        // sin'(x) = cos(x) for comparison
        auto cosx = torch::cos(x); 

        //std::cout << torch::eye(10) << std::endl;
        auto dsinx = torch::autograd::grad(
            // Jacobian is diagonal, so [1,1,1,1,...] spares computation.
            //{sinx}, {x}, {torch::ones_like(x)}, 
            {sinx}, {x}, {torch::eye(100)},
            /*create_graph=*/true, 
            /*allow_unused=*/true)[0].requires_grad_(true);
        
        double df_error = (cosx - dsinx).max().item<double>(); 
        assert(df_error == 0);

        auto ddsinx = torch::autograd::grad(
            {dsinx}, {x}, {torch::ones_like(x)}, 
            /*create_graph=*/true,
            /*allow_unused=*/true)[0].requires_grad_(true);

        // Save the data for visualization.
        torch::save(x, "x.pt");
        torch::save(sinx, "sinx.pt");
        torch::save(cosx, "cosx.pt");
        torch::save(dsinx, "dsinx.pt");
        torch::save(ddsinx, "ddsinx.pt");
        torch::save(-1*sinx, "dcosx.pt");
    }
}

