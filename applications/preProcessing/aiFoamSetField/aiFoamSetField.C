/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2020 2020 Tomislav Maric, TU Darmstadt
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    aiFoamSetField

Description
    Sets scalar values at cell centers or cell corners. 

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "pointFields.H"
#include "setFieldFunctions.H"
#include "implicitSurfaces.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    argList::addOption
    (
        "surfaceType",
        "Surface type name.",
        "plane, sphere, ellipsoid, sinc, sincScaled"
    );

    argList::addOption
    (
        "surfaceParams",
        "Surface parameters.",
        "Surface parameters:\
for a plane the position and normal '(px py pz) (nx ny nz)',\
for a sphere the center and radius '(cx cy cz) r',\
for an ellipsoid center and half-axes vectors '(cx cy cz) (ax ay az)'." 
    );

    argList::addOption
    (
        "volFieldName",
        "field name",
        "Name of the vol field to be calculated."
    );

    argList::addOption
    (
        "pointFieldName",
        "field name",
        "Name of the point field to be calculated."
    );


    argList::addBoolOption
    (
        "calcSignedDistance",
        "Use the foot-point (closest-point) algorithm to compute a signed distance field from a zero Level Set of an implicit function."
    );

    //TODO
    //argList::addOption
    //(
        //"maxIterations",
        //"integer",
        //"Maximal number of iterations for computing signed distances from surface points."
    //);

    //TODO
    //argList::addOption
    //(
        //"epsilon",
        //"scalar",
        //"L_inf error."
    //);

    #include "setRootCase.H"
    #include "createTime.H"
    #include "createMesh.H"

    pointMesh point_mesh(mesh);

    if (!args.found("surfaceType")) 
        FatalErrorInFunction
            << "Can't initialize a surface without -surfaceType." 
            << abort(FatalError);

    if (!args.found("volFieldName") && !args.found("pointFieldName"))
        FatalErrorInFunction
            << "Provide either -volFieldName or -pointFieldName." 
            << abort(FatalError);

    const auto surfaceType = args.get<word>("surfaceType");
    autoPtr<AI::implicitSurface> surfPtr = 
        AI::implicitSurface::New(surfaceType, args.lookup("surfaceParams"));
    const auto& surf = surfPtr.ref();

    Info << "Setting signed-distance for ..." << surf.type() << endl;

    setFields(args, mesh, point_mesh, surf);

    if (args.found("calcSignedDistance")) 
    {
        if (!args.found("volFieldName"))
            FatalErrorInFunction
                << "Distances only calculated at cell centers, use -volFieldName." 
                << abort(FatalError);

        auto volFieldName = args.get<word>("volFieldName");

        volScalarField sigDist(
            IOobject
            (
                "sig_dist",
                runTime.timeName(),
                mesh, 
                IOobject::NO_READ, 
                IOobject::AUTO_WRITE
            ), 
            mesh, 
            dimensionedScalar("sig_dist", dimLength, GREAT)
        );

        volScalarField sigDist0 ("sigDist0", sigDist); 

        // TODO
        //auto maxIt = args.lookupOrDefault<label>("maxIterations", 20);

        setSignedDistance(sigDist, surf); 
            
        sigDist.write();

        // Debug closest point  
        //volVectorField sigDistGrad 
        //(
            //IOobject
            //(
                //"sigDistGrad", 
                //runTime.timeName(), 
                //mesh, 
                //IOobject::NO_READ, 
                //IOobject::AUTO_WRITE
            //),
            //fvc::grad(sigDist)
        //); 
        //sigDistGrad.write();
        //volScalarField sigDistMagGrad 
        //(
            //IOobject
            //(
                //"sigDistMagGrad", 
                //runTime.timeName(), 
                //mesh, 
                //IOobject::NO_READ, 
                //IOobject::AUTO_WRITE
            //),
            //Foam::mag(sigDistGrad)
        //); 
        //sigDistMagGrad.write();

    }

    // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< nl;
    runTime.printExecutionTime(Info);

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
