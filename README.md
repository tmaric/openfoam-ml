# Machine Learning in OpenFOAM  

[![pipeline status](https://gitlab.com/tmaric/openfoam-ml/badges/master/pipeline.svg)](https://gitlab.com/tmaric/openfoam-ml/-/commits/master)

## Contributors 

* **Tomislav Marić** - *Maintainer/Developer* - [Contact](mailto:tomislav.maric@gmx.com)
* **Andre Weiner** - *Developer* - [Contact](mailto:a.weiner@tu-braunschweig.de)
* **Antonio Martín-Alcántara** - *Developer* - [Contact](mailto:a.martin.alcantara@gmail.com)
* **Niklas Bönisch** - *Developer/Tester* - [Contact](mailto:niklas.boenisch@stud.tu-darmstadt.de)

## Surface Approximation

Neural Networks for approximation of implicit surfaces on unstructured meshes.

## Deep Reinforcement Learning 

Deep Reinforcement Learning algorithms for solution control of coupled PDEs. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

* Python: Python 3.9.1, python-pandas, python-jupyter, python-matplotlib, python-numpy 
* OpenFOAM: v2012, v2006 
* libtorch: C++ API for pytorch, tested with 1.7.1 

### Installing

Install libtorch PyTorch C++ API either using a package manager, or download the archive from the [PyTorch website](https://pytorch.org/get-started/locally/) and extract the archive in the `openfoam-ml` folder. 

For example, get the latest CPU distribution of libtorch, 

```
    openfoam-ml> wget https://download.pytorch.org/libtorch/cpu/libtorch-cxx11-abi-shared-with-deps-1.7.1%2Bcpu.zip 
    openfoam-ml> unzip libtorch-cxx11-abi-shared-with-deps-1.7.1+cpu.zip
```

Configure variables used to include and link PyTorch files and libraries, 

```
    openfoam-ml> source setup-torch.sh
```

If not already set up, set up the OpenFOAM Environment 
```
    openfoam-ml> source /path/to/openfoam/etc/bashrc
```

Build `openfoam-ml` by calling 

```
?> ./Allwmake
```

## Running the tests

**TODO**: Explain how to run the automated tests for this system

### Break down into end to end tests

#### Surface approximation 

`unit_box_domain` test

```
?> blockMesh 
?> aiFoamLearnSurface
```

Fields with `nn` in their name are approximated by Neural Networks and can be visualized in ParaView. 

## Contributing

Contact the maintainer for the access to the project, report bugs and request new features [here](mailto:incoming+tmaric-openfoam-ml-19804613-issue-@incoming.gitlab.com). 

## License

This project is licensed under the GPL 3.0 License. 
